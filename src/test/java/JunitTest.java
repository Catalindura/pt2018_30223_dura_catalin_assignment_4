
import static org.junit.Assert.*;
import org.junit.*; 
public class JunitTest {
	private static Bank bank;
	private static int NumarTesteExecutate=0;
	private static int NumarTesteCuSucces=0;
	
	
	public JunitTest() {
		System.out.println("Constructor inaintea fiecarui test");
	}
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	 System.out.println("O singura data inaintea executiei setului de teste din clasa!");
	 bank = new Bank();
	 }
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	 System.out.println("O singura data dupa terminarea executiei setului de teste din clasa!");
	 System.out.println("S-au executat " + NumarTesteExecutate + " teste din care "+ NumarTesteCuSucces + " au avut succes!");
	 }
	@Before
	public void setUp() throws Exception {
	 System.out.println("Incepe un nou test!");
	 NumarTesteExecutate++;
	 }
	@After
	public void tearDown() throws Exception {
	 System.out.println("S-a terminat testul curent!");
}
	@Test
	public void testAdaugareClient() {
		bank.clear();
		Client adaugat=new Client("Catalin","0740238611","0987476583942");
		bank.addClient(adaugat);
		String s=bank.toString();
		assertNotNull(s);
		assertEquals(s,"{nume=Catalin telefon=0740238611 cnp=0987476583942=[]}" );
		NumarTesteCuSucces++;
	}
	@Test
	public void testStergereClient() {
		bank.clear();
		Client adaugat=new Client("Catalin","0740238611","0987476583942");
		bank.addClient(adaugat);
		bank.deleteClient(adaugat);
		String s=bank.toString();
		assertNotNull(s);
		assertEquals(s,"{}" );
		NumarTesteCuSucces++;
	}
	@Test
	public void testEditareClient() {
		bank.clear();
		Client vechi=new Client("Catalin","0740238611","0987476583942");
		Client nou=new Client("Alin","0740238611","0987476583942");
		bank.addClient(vechi);
		bank.editClient(vechi, nou);
		String s=bank.toString();
		assertNotNull(s);
		assertEquals(s,"{nume=Alin telefon=0740238611 cnp=0987476583942=[]}" );
		NumarTesteCuSucces++;
}	
	@Test
	public void testAdaugareCont() {
		bank.clear();
		Client adaugat=new Client("Catalin","0740238611","0987476583942");
		Account x=new SavingAccount(1,5000);
		bank.addClient(adaugat);
		bank.addAccount(adaugat, x);
		String s=bank.toString();
		assertNotNull(s);
		assertEquals(s,"{nume=Catalin telefon=0740238611 cnp=0987476583942=[id=1 sold=5000.0]}" );
		NumarTesteCuSucces++;
	}	
	
	@Test
	public void testStergereCont() {
		bank.clear();
		Client adaugat=new Client("Catalin","0740238611","0987476583942");
		Account x=new SavingAccount(1,5000);
		bank.addClient(adaugat);
		bank.addAccount(adaugat, x);
		bank.deleteAccount(adaugat,x.getId());
		String s=bank.toString();
		assertNotNull(s);
		assertEquals(s,"{nume=Catalin telefon=0740238611 cnp=0987476583942=[]}" );
		NumarTesteCuSucces++;
	}	
	
	@Test
	public void testEditareCont() {
		bank.clear();
		Client adaugat=new Client("Catalin","0740238611","0987476583942");
		Account x=new SavingAccount(1,5000);
		Account edit=new SavingAccount(1,5555);
		bank.addClient(adaugat);
		bank.addAccount(adaugat, x);
		bank.editAccount(adaugat, edit);
		String s=bank.toString();
		assertNotNull(s);
		assertEquals(s,"{nume=Catalin telefon=0740238611 cnp=0987476583942=[id=1 sold=5555.0]}" );
		NumarTesteCuSucces++;
	}	
	
	@Test
	public void testAdaugareSoldCont() {
		bank.clear();
		Client adaugat=new Client("Catalin","0740238611","0987476583942");
		Account x=new SavingAccount(1,5000);
		bank.addClient(adaugat);
		bank.addAccount(adaugat, x);
		bank.addSold(adaugat, x.getId(), 500);
		String s=bank.toString();
		assertNotNull(s);
		assertEquals(s,"{nume=Catalin telefon=0740238611 cnp=0987476583942=[id=1 sold=5500.0]}" );
		NumarTesteCuSucces++;
	}	
	
	@Test
	public void testretragereSoldCont() {
		bank.clear();
		Client adaugat=new Client("Catalin","0740238611","0987476583942");
		Account x=new SavingAccount(1,5000);
		bank.addClient(adaugat);
		bank.addAccount(adaugat, x);
		bank.withdraw(adaugat, x.getId(), 1000);
		String s=bank.toString();
		assertNotNull(s);
		assertEquals(s,"{nume=Catalin telefon=0740238611 cnp=0987476583942=[id=1 sold=4050.0]}" );
		NumarTesteCuSucces++;
	}	
	
	
	
	
}
