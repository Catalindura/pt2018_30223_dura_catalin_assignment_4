import java.io.Serializable;
import java.util.Observable;

import javax.swing.JOptionPane;

public class Client implements Observer,Serializable{
	private String nume;
	private String telefon;
	private String cnp;
	public Client(String n,String t,String c) {
		cnp=c;
		nume=n;
		telefon=t;
	}
	@Override
	public int hashCode() {
	int result=17;
	result=result*31+nume.hashCode();
	result=result*31+cnp.hashCode();
	System.out.println("Client hashCode:"+result+"");
	return result;
	}
	@Override
	public boolean equals(Object o) {
		System.out.println("Client equals");
		if(o==this) return true;
		if(!(o instanceof Client)) return false;
		Client c=(Client)o;
		return c.getNume().equals(nume) && c.getTelefon().equals(telefon) && c.getCnp().equals(cnp);
	}
	public String getNume() {
		return nume;
	}
	public String getTelefon() {
		return telefon;
	}
	public String getCnp() {
		return cnp;
	}
	public void setNume(String n) {
		nume=n;
	}
	public void setTelefon(String t) {
		telefon=t;
	}
	public void setCnp(String c) {
		cnp=c;
	}
	@Override
	public String toString() {
		System.out.println("Client string");
		return "nume="+this.nume+" "+"telefon="+this.telefon+" "+"cnp="+this.cnp;
	}
	public void update(String s) {
		s=s+nume;
		JOptionPane.showMessageDialog(null, s,"Actiune asupra contului", 0);
	}
}
