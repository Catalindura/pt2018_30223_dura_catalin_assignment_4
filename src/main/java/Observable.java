
public interface Observable {
	public void notifyObserver(String s);
	public void addObserver(Observer o);
	public void removeObserver(Observer o);
}
