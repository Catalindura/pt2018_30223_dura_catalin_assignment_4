
import java.awt.event.ActionListener;

import javax.swing.*;

public class ClientiView extends JFrame{
	private JButton adaugaClient=new JButton("Adauga client");
	private JButton stergeClient=new JButton("Sterge client");
	private JButton editeazaClient=new JButton("Editeaza client");
	private JButton listareClienti=new JButton("Listare clienti");
	private JTextField numeClient=new JTextField(15);
	private JTextField telefonClient=new JTextField(11);
	private JTextField cnpClient=new JTextField(13);
	private JTable t;
	public ClientiView() {
		JPanel p=new JPanel();
		p. setLayout (new BoxLayout (p, BoxLayout . Y_AXIS ));
		JPanel p1=new JPanel();
		p1.add(adaugaClient);
		p1.add(stergeClient);
		p1.add(editeazaClient);
		p1.add(listareClienti);
		JPanel p2=new JPanel();
		JLabel l1=new JLabel("Nume");
		p2.add(l1);
		p2.add(numeClient);
		JPanel p3=new JPanel();
		JLabel l2=new JLabel("Telefon");
		p3.add(l2);
		p3.add(telefonClient);
		JPanel p4=new JPanel();
		JLabel l3=new JLabel("CNP");
		p4.add(l3);
		p4.add(cnpClient);
		JPanel p5=new JPanel();
		p.add(p1);
		p.add(p2);
		p.add(p3);
		p.add(p4);
		this.setSize(500, 500);
		this.add(p);
		this.setVisible(true);
	}
	void adaugaTabel(JTable tabel) {
		t=tabel;
		JPanel p=new JPanel();
		p.add(t);
		this.add(p);
		this.repaint();
	}
	void showMessage(String s) {
		JOptionPane.showMessageDialog(this,s);
	}
	void adaugaClientListener(ActionListener mal) {
		adaugaClient.addActionListener(mal);
	}
	void stergeClientListener(ActionListener mal) {
		stergeClient.addActionListener(mal);
	}
	void editeazaClientListener(ActionListener mal) {
		editeazaClient.addActionListener(mal);
	}
	void listareClientiListener(ActionListener mal) {
		listareClienti.addActionListener(mal);
	}
	public String getNume() {
		return numeClient.getText();
	}
	public String getTelefon() {
		return telefonClient.getText();
	}
	public String getCnp() {
		return cnpClient.getText();
	}

}
