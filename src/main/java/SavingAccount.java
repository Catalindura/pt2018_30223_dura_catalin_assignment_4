import java.util.ArrayList;

public class SavingAccount extends Account{
	private ArrayList<Observer> observers=new ArrayList<Observer>();
	private double sold;
	private int id;
	public SavingAccount(int i,double s) {
		sold=s;
		id=i;
	}
	@Override
	public double add(double x) {
		sold=sold+x;
		notifyObserver("S-a adaugat suma "+x+" la SavingAccount-ul dumneavostra");
		return sold;
		
	}

	@Override
	public int withdraw(double x) {
		if (x<=sold){
			sold=sold+sold*0.01;
			sold=sold-x;
			notifyObserver("S-a extras suma "+x+" din SavingAccount-ul dumneavostra");
			return 1;
		}
		else
		return 0;
	}
	public void edit(SavingAccount x) {
		sold=x.getSold();
		id=x.getId();
		
	}
	@Override
	public double getSold() {
		return sold;
	}
	public int getId() {
		return id;
	}
	public String toString() {
		return "id="+id+" "+"sold="+sold;
	}
	@Override
	public void setId(int i) {
		id=i;
		
	}
	@Override
	public void setSold(double s) {
		sold=s;
		
	}
	public void notifyObserver(String s) {
		for(Observer o:observers)
			o.update(s);
	}
	public void addObserver(Observer o) {
		observers.add(o);
		
	}
	public void removeObserver(Observer o) {
		observers.remove(o);
	}
}
