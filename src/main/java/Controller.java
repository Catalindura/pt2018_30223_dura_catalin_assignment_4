import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;

import javax.swing.JTable;

public class Controller {
	Bank bank=new Bank();
	EntryView entry=new EntryView();
	ClientiView clienti;
	ConturiView conturi;
	TabelClienti tabelClienti;
	TabelConturi tabelConturi;
	Controller(){
		tabelClienti=new TabelClienti(bank.ClientJTable());
		tabelConturi=new TabelConturi(bank.AccountJTable());
		entry.conturiListener(new conturiListener());
		entry.clientListener(new clientiListener());
		entry.serializareListener(new serializareListener());
	}
	
	class conturiListener implements ActionListener{

		public void actionPerformed(ActionEvent e) {
		 conturi=new ConturiView();
		 conturi.adaugaContListener(new adaugaContListener());
		 conturi.stergeContListener(new stergeContListener());
		 conturi.editezaContListener(new editeazaContListener());
		 conturi.listareConturiListener(new listareConturiListener());
		 conturi.adaugareSumaListener(new adaugareSumaListener());
		 conturi.retragereSumaListener(new retragereSumaListnener());
		}
		
	}
	
	class clientiListener implements ActionListener{

		public void actionPerformed(ActionEvent e) {
		 clienti=new ClientiView();
		 clienti.adaugaClientListener(new adaugaClientListener());
		 clienti.stergeClientListener(new stergeClientListener());
		 clienti.editeazaClientListener(new editeazaClientListener());
		 clienti.listareClientiListener(new listareClientiListener());
		}
		
	}
	
	class serializareListener implements ActionListener{

		public void actionPerformed(ActionEvent e) {
			try {
				bank.serializare(bank.getMap());
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			
		}
		
	}
	
	
	class adaugaContListener implements ActionListener{

		public void actionPerformed(ActionEvent e) {
			Client adaugare=tabelClienti.getClientSelectat();
			Account x;
			int tip=conturi.tipCont();
			if(tip==0) {
				x=new SavingAccount(Integer.parseInt(conturi.idCont()),Integer.parseInt(conturi.soldInitial()));
			}
			else  x=new SpendingAccount(Integer.parseInt(conturi.idCont()),Integer.parseInt(conturi.soldInitial()));
			x.addObserver(adaugare);
			bank.addAccount(adaugare, x);
			tabelConturi.setVisible(false);
			tabelConturi=new TabelConturi(bank.AccountJTable());
		}
	
	}
	
	class stergeContListener implements ActionListener{

		public void actionPerformed(ActionEvent e) {
			Client titular=tabelClienti.getClientSelectat();
			Account x=tabelConturi.getConttSelectat();
			x.removeObserver(titular);
			bank.deleteAccount(titular, x.getId());
			tabelConturi.setVisible(false);
			tabelConturi=new TabelConturi(bank.AccountJTable());
			
		}
	}
	
	
	class editeazaContListener implements ActionListener{

		public void actionPerformed(ActionEvent e) {
			Client titular=tabelClienti.getClientSelectat();
			Account x;
			int tip=conturi.tipCont();
			if(tip==0) x=new SavingAccount(Integer.parseInt(conturi.idCont()),Integer.parseInt(conturi.soldInitial()));
			else  x=new SpendingAccount(Integer.parseInt(conturi.idCont()),Integer.parseInt(conturi.soldInitial()));
			bank.editAccount(titular,x);
			tabelConturi.setVisible(false);
			tabelConturi=new TabelConturi(bank.AccountJTable());
		}
		
	}
	
	class listareConturiListener implements ActionListener{

		public void actionPerformed(ActionEvent e) {
			tabelConturi=new TabelConturi(bank.AccountJTable());
			
		}
		
	}
	
	class adaugareSumaListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			Client titular=tabelClienti.getClientSelectat();
			Account x=tabelConturi.getConttSelectat();
			bank.addSold(titular, x.getId(),Integer.parseInt(conturi.sold()));
			tabelConturi.setVisible(false);
			tabelConturi=new TabelConturi(bank.AccountJTable());
			
		}
	}
	
	class retragereSumaListnener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			Client titular=tabelClienti.getClientSelectat();
			Account x=tabelConturi.getConttSelectat();
			bank.withdraw(titular, x.getId(),Integer.parseInt(conturi.sold()));
			tabelConturi.setVisible(false);
			tabelConturi=new TabelConturi(bank.AccountJTable());
		}
	}
	class adaugaClientListener implements ActionListener{

		public void actionPerformed(ActionEvent e) {
			Client adaugat=new Client(clienti.getNume(),clienti.getTelefon(),clienti.getCnp());
			bank.addClient(adaugat);	
			tabelClienti.setVisible(false);
			tabelClienti=new TabelClienti(bank.ClientJTable());
			}
		
	}
	class stergeClientListener implements ActionListener{

		public void actionPerformed(ActionEvent e) {
			Client sters=tabelClienti.getClientSelectat();
			bank.deleteClient(sters);
			tabelClienti.setVisible(false);
			tabelClienti=new TabelClienti(bank.ClientJTable());
		}
		
	}
	
	class editeazaClientListener implements ActionListener{

		public void actionPerformed(ActionEvent e) {
			Client adaugat=new Client(clienti.getNume(),clienti.getTelefon(),clienti.getCnp());
			Client editat=tabelClienti.getClientSelectat();
			System.out.println(editat.toString());
			bank.editClient(editat,adaugat);
			tabelClienti.setVisible(false);
			tabelClienti=new TabelClienti(bank.ClientJTable());
		}
		
	}
	
	class listareClientiListener implements ActionListener{

		public void actionPerformed(ActionEvent e) {
			 tabelClienti=new TabelClienti(bank.ClientJTable());
		}
		
	}
	public static void main(String[] args) {
		Controller view=new Controller();
	}
}
