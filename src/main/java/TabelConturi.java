
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.table.TableModel;

public class TabelConturi extends JFrame{
	private JTable tabel;
	public TabelConturi(JTable t){
		tabel=t;
		JScrollPane scroll=new JScrollPane(tabel);
		this.add(scroll);
		this.setSize(500,300);
		this.setVisible(true);
	}
	public Account getConttSelectat() {
		int index=tabel.getSelectedRow();
		Account x;
		TableModel model=tabel.getModel();
		String nume=model.getValueAt(index,0).toString();
		String idcont=model.getValueAt(index,1).toString();
		String sold=model.getValueAt(index,2).toString();
		String tip=model.getValueAt(index,3).toString();
		if(tip=="Saving")  x=new SavingAccount(Integer.parseInt(idcont),Double.parseDouble(sold));
		else  x=new SpendingAccount(Integer.parseInt(idcont),Double.parseDouble(sold));
		return x;
	}
	}
