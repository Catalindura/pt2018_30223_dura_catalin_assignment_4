import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JTable;

public class Bank implements BankProc{
private HashMap<Client,ArrayList<Account>> map=new HashMap<Client,ArrayList<Account>>();
	
	public Bank() {
		deserializare();
	}
	
	public static void serializare(HashMap<Client,ArrayList<Account>> m) throws IOException {
		FileOutputStream fileOut =
		         new FileOutputStream("banca.ser");
		         ObjectOutputStream out = new ObjectOutputStream(fileOut);
		         out.writeObject(m);
		         out.close();
		         fileOut.close();
		         System.out.printf("Serialized data is saved in banca.ser");
	}
	
	public void deserializare() {
		 try {
	         FileInputStream fileIn = new FileInputStream("banca.ser");
	         ObjectInputStream in = new ObjectInputStream(fileIn);
	         map = (HashMap<Client,ArrayList<Account>>) in.readObject();
	         in.close();
	         fileIn.close();
	      } catch (IOException i) {
	         i.printStackTrace();
	         return;
	      } catch (ClassNotFoundException c) {
	         c.printStackTrace();
	         return;
	      }
	}
	
	public void addClient(Client c) {
		assert (c!=null);
		assert(c.getCnp().length()==13);
		assert(c.getTelefon().length()==10);
		ArrayList<Account> lista=new ArrayList<Account>();
		map.put(c, lista);
	}
	

	public void editClient(Client vechi,Client nou) {
		assert (vechi!=null);
		assert (nou!=null);
		assert(nou.getCnp().length()==13);
		assert(nou.getTelefon().length()==10);
		ArrayList<Account> lista=new ArrayList<Account>();
		lista=map.get(vechi);
		map.remove(vechi);
		map.put(nou,lista);
	}

	public void deleteClient(Client c) {
		assert (map.get(c) != null);
		map.remove(c);
		assert(map.get(c)==null);
	}
	
	public int getNrClienti() {
		int i=0;
		for(Map.Entry<Client,ArrayList<Account>> pair:map.entrySet()) {
			i=i+1;
		}
		return i;
	}
	public JTable ClientJTable() {
		String[] coloana= {"Nume","Telefon","CNP"};
		String[][] matrice=new String[getNrClienti()][3];
		int i=0;
		for(Map.Entry<Client,ArrayList<Account>> pair:map.entrySet()) {
			Client x=pair.getKey();
			matrice[i][0]=x.getNume();
			matrice[i][1]=x.getTelefon();
			matrice[i][2]=x.getCnp();
			i=i+1;
		}
		JTable tabel=new JTable(matrice,coloana);
		return tabel;
		
	}

	public void addAccount(Client c,Account x) {
		assert (map.get(c)!=null);
		assert (x!=null);
		assert(x.getSold()>0);
		assert(x.getId()>0);
		map.get(c).add(x);
		
	}

	public void deleteAccount(Client c, int id) {
		int index=0,ok=0,i=0;
		assert (map.get(c)!=null);
		for(Account c1:map.get(c)) {
			if(c1.getId()==id) {
				ok=1;
				index=i;
			}
			i=i+1;
		}
		if (ok==1)
		map.get(c).remove(index);
	}

	public void editAccount(Client c,Account x) {
		assert (map.get(c)!=null);
		assert (x!=null);
		assert(x.getSold()>0);
		assert(x.getId()>0);
		for(Account c1:map.get(c)) {
			if(c1.getId()==x.getId()) {
				c1.setSold(x.getSold());
			}
	}
	}

	public int getNrConturi() {
		int i=0;
		for(Map.Entry<Client,ArrayList<Account>> pair:map.entrySet()) {
			i=i+pair.getValue().size();
		}
		return i;
	}
	
	public JTable AccountJTable() {
		String[] coloana= {"Nume titular","id cont","sold","tip cont"};
		String[][] matrice=new String[getNrConturi()][4];
		int i=0;
		for(Map.Entry<Client,ArrayList<Account>> pair:map.entrySet()) {
			Client c=pair.getKey();
			ArrayList<Account> aux=pair.getValue();
			for(Account x:aux) {
			matrice[i][0]=c.getNume();
			matrice[i][1]=String.valueOf(x.getId());
			matrice[i][2]=String.valueOf(x.getSold());
			if(x instanceof SavingAccount) matrice[i][3]="Saving";
			else matrice[i][3]="Spending";
			i=i+1;
		}
		
	}
		JTable tabel=new JTable(matrice,coloana);
		return tabel;
	}
	public String toString() {
		return map.toString();
	}
	
	
	public void addSold(Client c, int id, double x) {
		assert(map.get(c)!=null);
		assert(x>0);
		assert(id>-1);
		for(Account c1:map.get(c)) {
			if(c1.getId()==id) c1.add(x);	
		}
	}

	public void withdraw(Client c, int id, double x) {
		assert(map.get(c)!=null);
		assert(x>0);
		assert(id>-1);
		for(Account c1:map.get(c)) {
			if(c1.getId()==id) c1.withdraw(x);
		}
	}
	
	public HashMap<Client,ArrayList<Account>> getMap(){
		return map;
	}
	
	public void clear() {
		map.clear();
	}
	public static void main(String[] args) throws IOException {
		Bank b=new Bank();
		SavingAccount s1=new SavingAccount(1,3000);
		
	}

}
