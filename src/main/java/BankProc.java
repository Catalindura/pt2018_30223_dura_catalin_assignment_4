import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JTable;

public interface BankProc {
 	
/**
 * @pre cnp.size=13
 * @post map.get(c)
*/ 
	public void addClient(Client c) ;
/**
 * @pre cnp.size=13	
 * @post map.get(c)
 */
	public void editClient(Client vechi,Client nou);
/**
* @pre 	
* @post map.get(c)
*/
  public void deleteClient(Client c);

/**
* @pre map.get(c)
* @pre x.sold>0	
* 
*/
  public void addAccount(Client c,Account x);
 
/**
* @pre map.get(c)
* @post map.get(c).size=map.get(c)@pre.size-1	
* 
*/
  public void deleteAccount(Client c,int id);
/**
* @pre map.get(c)
* @pre x.sold>0	
* 
*/
  public void editAccount(Client c,Account x);

/**
 * @pre sold>0
 * @pre x>0
 * @post sold=x+@pre.sold
 */
  public void addSold(Client c,int id,double x);
/**
 * @pre sold>0
 * @post sold=@pre.sold-x
 */
  public void withdraw(Client c,int id,double x);
}

