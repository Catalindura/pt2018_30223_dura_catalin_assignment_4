
import java.awt.event.ActionListener;

import javax.swing.*;

public class EntryView extends JFrame{	
	private JButton conturi=new JButton("Conturi");
	private JButton clienti=new JButton("Clienti");
	private JButton serializare=new JButton("Salvare modificari");
	
	public EntryView() {
		JPanel p=new JPanel();
		p.add(conturi);
		p.add(clienti);
		p.add(serializare);
		this.add(p);
		this.setSize(300,300);
		this.setVisible(true);
	}
	void clientListener(ActionListener mal) {
		clienti.addActionListener(mal);
	}
	void conturiListener(ActionListener mal) {
		conturi.addActionListener(mal);
	}
	void serializareListener(ActionListener mal) {
		serializare.addActionListener(mal);
	}
	public static void main(String[] args) {
		EntryView view=new EntryView();
	}
}
