import java.io.Serializable;

public abstract class Account implements Observable,Serializable  {
	public abstract double add(double x);
	public abstract int withdraw(double x);
	public abstract int getId();
	public abstract double getSold();
	public abstract void setId(int i);
	public abstract void setSold(double s);
	public abstract void notifyObserver(String s);
	public abstract void addObserver(Observer o);
	public abstract void removeObserver(Observer o);
}
