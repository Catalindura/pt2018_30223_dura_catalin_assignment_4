
import java.awt.event.ActionListener;

import javax.swing.*;

public class ConturiView extends JFrame {
	private JButton adaugaCont=new JButton("Adauga cont");
	private JButton stergeCont=new JButton("Sterge cont");
	private JButton editeazaCont=new JButton("Editeaza cont");
	private JButton listareConturi=new JButton("Listare conturi");
	private JButton adaugaSuma=new JButton("Adauga suma");
	private JButton retrageSuma=new JButton("Retrage suma");
	private String[] list= {"Saving","Spending"};
	private JComboBox optiuneCont=new JComboBox(list);
	private JTextField idCont=new JTextField(3);
	private JTextField soldCont=new JTextField(6);
	private JTextField sold=new JTextField(6);
	public ConturiView() {
		JPanel p=new JPanel();
		p. setLayout (new BoxLayout (p, BoxLayout . Y_AXIS ));
		JPanel p1=new JPanel();
		p1.add(adaugaCont);
		p1.add(stergeCont);
		p1.add(editeazaCont);
		p1.add(listareConturi);
		JPanel p2=new JPanel();
		JLabel l1=new JLabel("idCont");
		JLabel l2=new JLabel("Sold");
		p2.add(optiuneCont);
		p2.add(l1);
		p2.add(idCont);
		p2.add(l2);
		p2.add(soldCont);
		JPanel p3=new JPanel();
		p3.add(adaugaSuma);
		p3.add(retrageSuma);
		p3.add(sold);
		p.add(p1);
		p.add(p2);
		p.add(p3);
		this.setSize(500,500);
		this.add(p);
		this.setVisible(true);
	}
	void adaugaContListener(ActionListener mal) {
		adaugaCont.addActionListener(mal);
	}
	void stergeContListener(ActionListener mal) {
		stergeCont.addActionListener(mal);
	}
	void editezaContListener(ActionListener mal) {
		editeazaCont.addActionListener(mal);
	}
	void listareConturiListener(ActionListener mal) {
		listareConturi.addActionListener(mal);
	}
	void adaugareSumaListener(ActionListener mal) {
		adaugaSuma.addActionListener(mal);
	}
	void retragereSumaListener(ActionListener mal) {
		retrageSuma.addActionListener(mal);
	}
	public String idCont() {
		return idCont.getText();
	}
	public String soldInitial() {
		return soldCont.getText();
	}
	public String sold() {
		return sold.getText();
	}
	public int tipCont() {
		return optiuneCont.getSelectedIndex();
	}
	void showMessage(String s) {
		JOptionPane.showMessageDialog(this,s);
	}
	public static void main(String[] args) {
		ConturiView view=new ConturiView();
	}
}
