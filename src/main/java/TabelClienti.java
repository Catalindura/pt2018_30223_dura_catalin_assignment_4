
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.table.TableModel;

public class TabelClienti extends JFrame{
	private JTable tabel;
	public TabelClienti(JTable t){
		tabel=t;
		JScrollPane scroll=new JScrollPane(tabel);
		this.add(scroll);
		this.setSize(500,300);
		this.setVisible(true);
	}
	public Client getClientSelectat() {
		int index=tabel.getSelectedRow();
		TableModel model=tabel.getModel();
		String nume=model.getValueAt(index,0).toString();
		String telefon=model.getValueAt(index,1).toString();
		String cnp=model.getValueAt(index,2).toString();
		Client selectat=new Client(nume,telefon,cnp);
		return selectat;
	}
	}
