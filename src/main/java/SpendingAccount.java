import java.util.ArrayList;

public class SpendingAccount extends Account{
	private ArrayList<Observer> observers=new ArrayList<Observer>();
	private int id;
	private double sold;
	public SpendingAccount(int i,double s) {
		id=i;
		sold=s;
	}
	@Override
	public double add(double x) {
		sold=sold+x;
		notifyObserver("S-a adaugat suma de "+x+" din SpendingAccount-ul dumneavostra");
		return sold;
	}

	@Override
	public int withdraw(double x) {
		if (x<=sold) {
			sold=sold-x;
			notifyObserver("S-a extras suma de "+x+" din SpendingAccount-ul dumneavostra");
			return 1;
		}
		else
		return 0;
	}
	public void edit(SpendingAccount x) {
		id=x.getId();
		sold=x.getSold();

	}
	public int getId() {
		return id;
	}
	@Override
	public double getSold() {
		return sold;
	}
	public String toString() {
		return "id="+id+" "+"sold="+sold;
	}
	@Override
	public void setId(int i) {
		id=i;
	}
	@Override
	public void setSold(double s) {
		sold=s;
	}
	@Override
	public void notifyObserver(String s) {
		for(Observer o:observers)
			o.update(s);
		
	}
	@Override
	public void addObserver(Observer o) {
		observers.add(o);
	}
	@Override
	public void removeObserver(Observer o) {
		observers.remove(o);
	}
}
